import React, { Component } from 'react';
import '../Css/style.css';

class Test extends Component {
    state = { 
        result : [],
        input : '',
        oneResult : []
     }

     getName(event){
         this.setState({
            input : event.target.value
         })
     }
    
     getAll(){
         fetch('https://api.themoviedb.org/3/search/multi?api_key=34e6fc3c81ac8eabe5047223e851f6c5&language=fr&query='+this.state.input+'&page=1&include_adult=false')
         .then((resp)=>{
             resp.json()
             .then((res)=>{
                 this.setState({
                     result : res.results
                 },()=>console.log(this.state))
             })
         })
     }

     getOne(event){
         fetch('https://api.themoviedb.org/3/movie/'+event.target.textContent+'?api_key=34e6fc3c81ac8eabe5047223e851f6c5&language=fr-FR')
         .then((resp)=>{
             resp.json()
             .then((res)=>{
                 this.setState({
                    oneResult : res
                 })
             })
         })
     }

     renderAll(){
         return this.state.result.map((item)=>{
             return(
                <div 
                className="content" 
                key={item.id}
                >
                <img src={item.poster_path!==undefined?' http://image.tmdb.org/t/p/w300/'+item.poster_path:'https://d2tbfnbweol72x.cloudfront.net/wp-content/plugins/marketpress/marketpress-includes/images/default-product.png'} alt=""/>
                <div className="subContent">
                <h3>{item.title!==undefined?item.title:item.original_name}</h3>
                <p>{item.overview}</p>
                </div>
                </div>   
             )
         })
     }

     renderOne(){
        return (
            <div className="moreInfo">
            <img 
            className = "imgMoreInfo"
            src={this.state.oneResult.poster_path!==undefined?' http://image.tmdb.org/t/p/w300/'+this.state.oneResult.poster_path:'https://d2tbfnbweol72x.cloudfront.net/wp-content/plugins/marketpress/marketpress-includes/images/default-product.png'}
            alt=''
            />
            <p>{this.state.oneResult.overview}</p>
            </div>
        )
     }

    render() { 
        return (
            <div className="containt">
                <div className="buttonInside">
                    <button
                        onClick={this.getAll.bind(this)}
                        >
                        <i className="fas fa-search"></i>
                    </button>
                    <input
                    onKeyDown={(event)=>(event.keyCode===13)?this.getAll():''}
                    type="text" 
                    name="jacket"
                    value={this.state.input}
                    onChange={this.getName.bind(this)}
                    placeholder="Rechercher jaquettes"
                    autoFocus="true"
                    />
                </div>
                    <div className="allJaq">
                

                        {this.renderAll()}
                    </div> 
            </div>
        );
    }
}
 
export default Test;